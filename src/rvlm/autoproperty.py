"""

@autoproperty
def f(self): pass

@autoproperty
def f(): pass

@autoproperty(getter=True)
def f(self):
    return self.something + something

@autoproperty(setter=True)
def f(self, ...): pass

@autoproperty(alias=True)
def f(self, ...): return self.other

class AutoPropertyError(RuntimeError):
    pass
"""

import inspect

def autoproperty(getter=False, setter=False, alias=False):
    """
    """
    def decorator(co):
        # Alias for code without line breaks.
        Error = AutoPropertyError

        # Get property name for futher internal use. This name will be part
        # of automatic “private” variable.
        name = getattr(co, "__name__", None)
        if name is None:
            raise Error("Callable object must define __name__.")

        # Inspect arguments in order to decide if function is eligible for
        # automatic property trasformation.
        (args, varargs, varkw) = inspect.getargs(co)
        if len(varargs) != 0 or len(varkw) != 0:
            raise Error("Callable object mustn't have *varargs or **varkw.")

        # Cache for performance. Comment for beauty.
        argc = len(args)

        # Used as @autoproperty() ...
        if not (getter or setter or alias):
            # ... on f().
            if argc == 0:
                return property()

            # ... on f(self).
            if argc == 1 and args[0] == "self":
                return property()
       
            raise Error(
                "Wrong parameters for full-automatic mode: only 'f()' "
                "and 'f(self)' are allowed, with 'self' spelled exactly.")

        # Used as @autoproperty(getter=True) ...
        if getter and not (setter or alias):
            # ... on f().
            if argc == 0:
                return property()

            # ... on f(self).
            if argc == 1 and args[0] == "self":
                return property()

            raise Error(
                "Wrong parameters for 'getter' mode: only 'f()' and 'f(self)' "
                "signatures are allowed, with 'self' spelled exactly")

        # Used as @autoproperty(setter=True) ...
        if setter and not (getter or alias):
            # ... on f(arg).
            if argc == 1 and args[0] != "self":
                pass 

            # ... on f(self, arg).
            if argc == 2 and args[0] == "self":
                pass

            raise Error(
                "Wrong parameters for 'setter' mode: only 'f(val)' "
                "and 'f(self, val)' signatures are allowed, with 'self' "
                "spelled exactly")

        # Used as @autoproperty(alias=True)
        if alias and not (getter or setter):
            raise Error("No support for 'alias' for now.")

        # In none above matched, usage pattern is wrong.
        raise Error(
            "Only one of 'getter', 'setter' and 'alias' mode parameters can be"
            "set at the same time")

    # -----
    # Scroll up if you've already forgotten what 'decorator' is. Thank you Guido
    # for not having currying in Python.
    return decorator
